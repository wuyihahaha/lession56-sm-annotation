package com.wuyi.lession56.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wuyi.lession56.dao.UserMapper;
import com.wuyi.lession56.entity.User;
import com.wuyi.lession56.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TODO
 * Created by wy on 2021/11/15
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public PageInfo<User> getByPage(int pageIndex, int pageSize) {
        PageHelper.startPage(pageIndex,3);
        List<User> users = userMapper.getAll();
        PageInfo<User> pageInfo = PageInfo.of(users);
        return pageInfo;
    }

    @Override
    public List<User> getAll() {
        return userMapper.getAll();
    }
}
