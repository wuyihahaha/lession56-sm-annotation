package com.wuyi.lession56.service;

import com.github.pagehelper.PageInfo;
import com.wuyi.lession56.entity.User;

import java.util.List;

/**
 * TODO
 * Created by wy on 2021/11/15
 */
public interface UserService {
    List<User> getAll();
    PageInfo<User> getByPage(int pageIndex,int pageSize);
}
