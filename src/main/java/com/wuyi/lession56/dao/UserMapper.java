package com.wuyi.lession56.dao;

import com.wuyi.lession56.entity.User;

import java.util.List;

/**
 * TODO
 * Created by wy on 2021/11/15
 */
public interface UserMapper {
    List<User> getAll();
}
