package com.wuyi.lession56.entity;


public class User {

  private int uid;
  private String username;
  private String password;
  private String email;
  private String gender;
  private java.sql.Date registerTime;
  private java.sql.Date lastlogin;


  public int getUid() {
    return uid;
  }

  public void setUid(int uid) {
    this.uid = uid;
  }

  @Override
  public String toString() {
    return "User{" +
            "uid=" + uid +
            ", username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", email='" + email + '\'' +
            ", gender='" + gender + '\'' +
            ", registerTime=" + registerTime +
            ", lastlogin=" + lastlogin +
            '}';
  }

  public User() {
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }


  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }


  public java.sql.Date getRegisterTime() {
    return registerTime;
  }

  public void setRegisterTime(java.sql.Date registerTime) {
    this.registerTime = registerTime;
  }


  public java.sql.Date getLastlogin() {
    return lastlogin;
  }

  public void setLastlogin(java.sql.Date lastlogin) {
    this.lastlogin = lastlogin;
  }

}
