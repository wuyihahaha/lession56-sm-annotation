import com.github.pagehelper.PageInfo;
import com.wuyi.lession56.entity.User;
import com.wuyi.lession56.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * TODO
 * Created by wy on 2021/11/15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath*:application.xml")
public class Demo01 {
    @Autowired
    private UserService userService ;
    @Test
    public void test01(){
        userService.getAll().stream().forEach(System.out::println);
    }

    @Test
    public void test(){
        PageInfo<User> pages = userService.getByPage(1, 3);
        pages.getList().stream().forEach(System.out::println);
    }

}
